﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Workshop.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InsertOrder()
        {
            return View();
        }
        public ActionResult UpdateOrder()
        {
            return View();
        }
    }
}