﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Workshop.Models;
using System.Text;
using Newtonsoft.Json;
using Workshop.ViewModels;

namespace Workshop.Controllers
{
    public class OrderController : Controller
    {
        string connstr = ConfigurationManager.ConnectionStrings["Workshop"].ConnectionString;
        
       /// <summary>
       /// XXSSS
       /// </summary>
       /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            //DataSet dbOrder = new DataSet();
           
            List<IndexOrders> indices = new List<IndexOrders>();
            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string sqlcommand = @"SELECT ord.OrderID
                                            ,cus.CompanyName
                                            ,ord.OrderDate
                                            ,ord.ShippedDate
                                            ,ord.Freight
                                      FROM [WorkShop].[dbo].[Sales_Orders] as ord,[dbo].[Sales_Customers] as cus
                                      WHERE ord.CustomerID=cus.CustomerID ";

                //SqlDataAdapter adapter = new SqlDataAdapter(sqlcommand, connstr);
                //adapter.Fill(dbOrder.IndexOrders);
                SqlCommand command = new SqlCommand(sqlcommand, sqlcon);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    IndexOrders order = new IndexOrders()
                    {
                        OrderId = Convert.ToInt32(reader["OrderID"].ToString()),
                        CompanyName = reader["CompanyName"].ToString(),
                        OrderDate = Convert.ToDateTime(reader["OrderDate"].ToString()),
                        ShippedDate = Convert.ToDateTime(reader["ShippedDate"].ToString()),
                        Feight = Convert.ToDecimal(reader["Freight"].ToString())
                    };
                    indices.Add(order);
                }
                sqlcon.Close();
            }

            IndexViewModel dbOrder = new IndexViewModel()
            {
                IndexOrders = indices
            };
            return View(dbOrder);
        }

        [HttpPost]
        public ActionResult GetGrid()
        {
            List<IndexOrders> indices = new List<IndexOrders>();
            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string sqlcommand = @"SELECT ord.OrderID
                                            ,cus.CompanyName
                                            ,ord.OrderDate
                                            ,ord.ShippedDate
                                            ,ord.Freight
                                      FROM [WorkShop].[dbo].[Sales_Orders] as ord,[dbo].[Sales_Customers] as cus
                                      WHERE ord.CustomerID=cus.CustomerID ";

                //SqlDataAdapter adapter = new SqlDataAdapter(sqlcommand, connstr);
                //adapter.Fill(dbOrder.IndexOrders);
                SqlCommand command = new SqlCommand(sqlcommand, sqlcon);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    IndexOrders order = new IndexOrders()
                    {
                        OrderId = Convert.ToInt32(reader["OrderID"].ToString()),
                        CompanyName = reader["CompanyName"].ToString(),
                        OrderDate = Convert.ToDateTime(reader["OrderDate"].ToString()),
                        ShippedDate = Convert.ToDateTime(reader["ShippedDate"].ToString()),
                        Feight = Convert.ToDecimal(reader["Freight"].ToString()),

                    };
                    indices.Add(order);
                }
                sqlcon.Close();
            }
            return Content(JsonConvert.SerializeObject(indices));
        }

        [HttpPost]
        public ActionResult GetCompanyName()
        {
            List<Customer> customers = new List<Customer>();
            StringBuilder sb = new StringBuilder();

            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string sqlcommand = @"SELECT [CompanyName],[CustomerID]
                                      FROM [WorkShop].[dbo].[Sales_Customers]";
                SqlCommand command = new SqlCommand(sqlcommand, sqlcon);
                SqlDataReader reader = command.ExecuteReader();
                
                while (reader.Read())
                {
                    Customer customer = new Customer()
                    {
                        CompanyName = reader["CompanyName"].ToString(),
                        CustomerId = Convert.ToInt32(reader["CustomerID"].ToString())
                    };

                    customers.Add(customer);

                }
               
                sqlcon.Close();
                
            }
            
            return Content(JsonConvert.SerializeObject(customers));
        }

        [HttpPost]
        public ActionResult GetEmployeeName()
        {
            List<Employee> employees = new List<Employee>();
            StringBuilder sb = new StringBuilder();

            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string sqlcommand = @"SELECT [EmployeeID],[FirstName],[LastName]
                                      FROM [WorkShop].[dbo].[HR_Employees]";

                SqlCommand command = new SqlCommand(sqlcommand, sqlcon);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Employee employee = new Employee()
                    {
                        EmployeeName = reader["FirstName"].ToString()+" "+reader["LastName"].ToString(),
                        EmployeeId = Convert.ToInt32(reader["EmployeeID"].ToString())
                    };

                    employees.Add(employee);

                }

                sqlcon.Close();

            }

            return Content(JsonConvert.SerializeObject(employees));
        }

        [HttpPost]
        public ActionResult Create(OrdersModel orders)
        {
            //int maxId = 0;
            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string searchquery = @"Insert Into[dbo].[Sales_Orders] " +
                                      "Values((Select Max(OrderID)+1 From [WorkShop].[dbo].[Sales_Orders])," +
                                             "@CustomerId," +                  
                                             "@EmployeeId," +                  
                                             "@OrderDate," +                   
                                             "@RequiredDate," +                
                                             "@ShippedDate," +                 
                                             "@ShippedId," +                   
                                             "@Feight," +                      
                                             "@ShipAddress," +                 
                                             "@ShipCity," +                    
                                             "@ShipRegion," +                  
                                             "@ShipPostalCode," +              
                                             "@ShipCountry)";                  
                SqlCommand command = new SqlCommand(searchquery, sqlcon);
                //SqlDataReader reader = command.ExecuteReader();
                //maxId = Convert.ToInt32(reader);
                //orders.OrderId = maxId + 1;
                //reader.NextResult();
                //command.Parameters.AddWithValue("@OrderId", orders.OrderId);
                command.Parameters.AddWithValue("@CustomerId", orders.CustomerId);
                command.Parameters.AddWithValue("@EmployeeId", orders.EmployeeId);
                command.Parameters.AddWithValue("@OrderDate", orders.OrderDate);
                command.Parameters.AddWithValue("@RequiredDate", orders.RequiredDate);
                command.Parameters.AddWithValue("@ShippedDate", orders.ShippedDate);
                command.Parameters.AddWithValue("@ShippedId", orders.ShipperId);
                command.Parameters.AddWithValue("@Feight", orders.Feight);
                command.Parameters.AddWithValue("@ShipAddress", orders.ShipAddress??"");
                command.Parameters.AddWithValue("@ShipCity", orders.ShipCity??"");
                command.Parameters.AddWithValue("@ShipRegion", orders.ShipRegion??"");
                command.Parameters.AddWithValue("@ShipPostalCode", orders.ShipPostalCode??"");
                command.Parameters.AddWithValue("@ShipCountry", orders.ShipCountry??"");
                try
                {
                    command.ExecuteNonQuery();
                    sqlcon.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    sqlcon.Close();
                }
            }

            return RedirectToAction("Index");
        }

                                                                                    
        // GET: Order/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)                                            
        {
            OrdersModel ordersModel = new OrdersModel();
            DataTable table = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string editquery = "Select * From [dbo].[Sales_Orders] Where OrderId = @OrderId";
                SqlDataAdapter adapter = new SqlDataAdapter(editquery, sqlcon);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", id);
                adapter.Fill(table);
                sqlcon.Close();
            }
            if (table.Rows.Count == 1)
            {
                ordersModel.OrderId = id;
                ordersModel.CustomerId = Convert.ToInt32(table.Rows[0][1].ToString());
                ordersModel.EmployeeId = Convert.ToInt32(table.Rows[0][2].ToString());
                ordersModel.OrderDate = Convert.ToDateTime(table.Rows[0][3].ToString());
                ordersModel.RequiredDate= Convert.ToDateTime(table.Rows[0][4].ToString());
                ordersModel.ShippedDate= Convert.ToDateTime(table.Rows[0][5].ToString());
                ordersModel.ShipperId= Convert.ToInt32(table.Rows[0][6].ToString());
                ordersModel.Feight = Convert.ToDecimal(table.Rows[0][7].ToString());
                ordersModel.ShipAddress = table.Rows[0][8].ToString();
                ordersModel.ShipCity= table.Rows[0][9].ToString();
                ordersModel.ShipRegion = table.Rows[0][10].ToString();
                ordersModel.ShipPostalCode= table.Rows[0][11].ToString();
                ordersModel.ShipCountry = table.Rows[0][12].ToString();

                return View(ordersModel);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }                                                                           
                                                                                    
        // POST: Order/Edit/5                                                       
        [HttpPost]
        public ActionResult Edit(OrdersModel orders)
        {
            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string updatequery = "Update [dbo].[Sales_Orders] " +
                                     "Set CustomerId=@CustomerId," +
                                         "EmployeeId=@EmployeeId," +
                                         "OrderDate=@OrderDate," +
                                         "RequiredDate=@RequiredDate," +
                                         "ShippedDate=@ShippedDate," +
                                         "ShipperID=@ShippedId," +
                                         "Freight=@Feight," +
                                         "ShipAddress=@ShipAddress," +
                                         "ShipCity=@ShipCity," +
                                         "ShipRegion=@ShipRegion," +
                                         "ShipPostalCode=@ShipPostalCode," +
                                         "ShipCountry=@ShipCountry Where OrderId = @OrderId";
                SqlCommand command = new SqlCommand(updatequery, sqlcon);
                command.Parameters.AddWithValue("@OrderId", orders.OrderId);
                command.Parameters.AddWithValue("@CustomerId", orders.CustomerId);
                command.Parameters.AddWithValue("@EmployeeId", orders.EmployeeId);
                command.Parameters.AddWithValue("@OrderDate", orders.OrderDate);
                command.Parameters.AddWithValue("@RequiredDate", orders.RequiredDate);
                command.Parameters.AddWithValue("@ShippedDate", orders.ShippedDate);
                command.Parameters.AddWithValue("@ShippedId", orders.ShipperId);
                command.Parameters.AddWithValue("@Feight", orders.Feight);
                command.Parameters.AddWithValue("@ShipAddress", orders.ShipAddress);
                command.Parameters.AddWithValue("@ShipCity", orders.ShipCity);
                command.Parameters.AddWithValue("@ShipRegion", orders.ShipRegion);
                command.Parameters.AddWithValue("@ShipPostalCode", orders.ShipPostalCode);
                command.Parameters.AddWithValue("@ShipCountry", orders.ShipCountry);
                try
                {
                    command.ExecuteNonQuery();
                    sqlcon.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    sqlcon.Close();
                }
            }
            return RedirectToAction("Index");
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int id)
        {
            using (SqlConnection sqlcon = new SqlConnection(connstr))
            {
                sqlcon.Open();
                string updatequery = "Delete From [dbo].[Sales_Orders] Where OrderId = @OrderId";
                SqlCommand command = new SqlCommand(updatequery, sqlcon);
                command.Parameters.AddWithValue("@OrderId", id);
              
                try
                {
                    command.ExecuteNonQuery();
                    sqlcon.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    sqlcon.Close();
                }
            }
            return RedirectToAction("Index");
        }

    }
}
