﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workshop.Models;
namespace Workshop.ViewModels
{
    public class IndexViewModel
    {
        public OrdersModel Orders { get; set; }
        public IEnumerable<IndexOrders> IndexOrders { get; set; }
    }
}